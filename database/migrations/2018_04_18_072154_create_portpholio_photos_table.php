<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortpholioPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portpholio_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portpholio_item_id')->unsigned()->index()->default(0);
            $table->integer('user_id')->unsigned()->index();
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portpholio_photos');
    }
}
