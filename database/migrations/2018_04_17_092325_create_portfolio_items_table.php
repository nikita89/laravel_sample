<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portpholio_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('category_id')->unsigned()->index()->default(0)->nullable();
            $table->string('name');
            $table->text('body');
            $table->text('options');
            $table->string('seo_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('slug')->unique();
            $table->text('meta_description')->nullable();
            $table->integer('is_active')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_items');
    }
}
