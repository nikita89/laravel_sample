<?php

namespace App;

use TCG\Voyager\Models\Category;

class PortpholioCategory extends Category
{


    public function parent(){
        return $this->belongsTo('App\PortpholioCategory', 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\PortpholioCategory', 'parent_id');
    }

    public function portpholio(){
        return $this->hasMany('App\PortpholioItem');
    }
}
