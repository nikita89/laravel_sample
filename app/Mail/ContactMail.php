<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $input = '';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this -> input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->to('viperoussnake@yandex.ru');
        $this->subject('new message');
        return $this->view('mail');
    }
}
