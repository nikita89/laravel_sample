<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBreadController;
use Voyager;
use App\PortpholioPhoto;
use App\PortpholioItem;
class PortpholioItemController extends VoyagerBreadController
{
    // Add a new item of our Data Type BRE(A)D
    //
    //****************************************

    public function create(Request $request)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        \Storage::disk('public')->delete('portpholio-photo/0/1/LeUDIC1aV7M.jpg');

        if (view()->exists("admin.$slug.edit-add")) {
            $view = "admin.$slug.edit-add";
        }

        $photos = $this -> notBindedPhotos();

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'photos'));
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $user_id= \Auth::user()->id;
            $input = $request->all();
            $filledoption = false;
            $options = $request->options;
            if(is_array($options)){
                $options = array_filter($options, function($option){
                    return !is_null($option);
                });
                if(!empty($options))
                    $input['options'] = json_encode($options);
                else
                    unset($input['options']);
            }
/*
            $translations = is_bread_translatable($data)
                ? $data->prepareTranslations($request)
                : [];

            if (count($translations) > 0) {
                $data->saveTranslations($translations);
            }
*/
            $input['user_id'] = $user_id;
            $portpholio_post = PortpholioItem::create($input);
            $photos = PortpholioPhoto::where(['user_id' => $user_id, 'portpholio_item_id' => 0])
                ->get();
                //->update(['portpholio_item_id' => $portpholio_post->id]);
            if(count($photos) > 0){
                foreach ($photos as $photo){
                    $photo->portpholio_item()->associate($portpholio_post);
                    $final_path = $slug.'/'.$portpholio_post->id.'/'.$photo->name;
                    $this->udatePhotoPath($photo, $final_path);
                    $photo->save();
                }
            }

            $this -> saveTranslation($request, $portpholio_post);

            return redirect()->route("voyager.{$dataType->slug}.index");

        }


    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        $dataTypeContent = app($dataType->model_name)->with(['photos'])->findOrFail($id);

        $photos = $dataTypeContent->photos;
        if(count($photos) > 0){
            $photos = '<script>var existingphotos ='.$photos->toJson().'</script>';
        }
        else
            $photos = '';


        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("admin.$slug.edit-add")) {
            $view = "admin.$slug.edit-add";
        }

        $photos = $this -> notBindedPhotos($id);

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'photos'));
    }


    public function storephoto(Request $request){
        if ($request->hasFile('file')){
            $input = $request->all();
            $user_id= \Auth::user()->id;
            $input['user_id'] = $user_id;
            $input['portpholio_item_id'] = 0;

            $slug = $this->getSlug($request);

            $file = $request -> file;
            $extension = $file -> getClientOriginalExtension();
            $originalname = preg_replace("/".$extension."/iu", '', $file -> getClientOriginalName());
            $filename = \Transliterate::make($originalname).'.'.$extension;
            $image = \Image::make($file)->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })-> encode();
            $path = \Storage::disk('public')->put($slug.'/0/'.$user_id.'/'.$filename, $image);
            $imageurl = $slug.'/0/'.$user_id.'/'.$filename;
            $filesize = \Storage::disk('public')->size($slug.'/0/'.$user_id.'/'.$filename);
            $input['name'] = $filename;
            $input['path'] = $imageurl;
            $input['size'] = $filesize;

            $photo = PortpholioPhoto::create($input);
            return $photo->id;
        }

    }

    public function deletephoto(Request $request, PortpholioPhoto $photo){
        $this->authorize('delete', $photo);
        $photo->delete();
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $portpholio_post = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $portpholio_post);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $slug, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $user_id= \Auth::user()->id;
            $input = $request->all();
            $options = $request->options;
            $newoptions = array();
            if(is_array($options)){
                $options = array_filter($options, function($option){
                    return !is_null($option);
                });

                if(!empty($options))
                    $input['options'] = json_encode($options);
                else
                    unset($input['options']);
            }




            $input['user_id'] = $user_id;
            $portpholio_post -> fill($input) -> save();
            $photos = PortpholioPhoto::where(['user_id' => $user_id, 'portpholio_item_id' => 0])
                ->get();
            //->update(['portpholio_item_id' => $portpholio_post->id]);
            if(count($photos) > 0){
                foreach ($photos as $photo){
                    $photo->portpholio_item()->associate($portpholio_post);
                    $final_path = $slug.'/'.$portpholio_post->id.'/'.$photo->name;
                    $this->udatePhotoPath($photo, $final_path);
                    $photo->save();
                }
            }

            $this -> saveTranslation($request, $portpholio_post);

            return redirect()->route("voyager.{$dataType->slug}.index");
        }


    }

    protected function udatePhotoPath(PortpholioPhoto $photo, $path){
        $disk = \Storage::disk('public');
        $old_path = ($photo->getAttributes())['path'];
        \Storage::disk('public')->move($old_path, $path);
        $photo->path = $path;
    }

    protected function notBindedPhotos ($withpost = 0){
        $user_id= \Auth::user()->id;
        //$photos = '';
        $photos = PortpholioPhoto::where(
            function($query) use($user_id, $withpost){
                $query->where(['user_id' => $user_id])
                    ->where(function($query) use($withpost) {
                        if($withpost){
                            $query->where(['portpholio_item_id' => $withpost])->
                            orWhere(['portpholio_item_id' => 0]);
                        }
                        else
                            $query->where(['portpholio_item_id' => 0]);
                    });
            }) -> select('id', 'path', 'size', 'name')->get();

        if(count($photos) > 0){
            $photos = '<script>var existingphotos ='.$photos->toJson().'</script>';
        }
        else
            $photos = '';

        return $photos;
    }

    protected function saveTranslation($request, $data){
        $translations = is_bread_translatable($data)
            ? $data->prepareTranslations($request)
            : [];

        if (count($translations) > 0) {
            $data->saveTranslations($translations);
        }
    }

}
