<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;


class PortpholioPhotoPolicy extends BasePolicy
{

    public function delete(User $user, $model){

        return $user->id === $model->user_id;

    }


}
