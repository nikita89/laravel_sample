<?php

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\DataType;

if (! function_exists('getFieldById')) {

    function getFieldById($id, $model, $field = 'slug')
    {
        $model_inst = (new $model()) -> findOrFail($id);
        return $model_inst -> slug;
    }
}


if (! function_exists('filterCrumb')) {

    function filterCrumb(Model $model, Model $parent, $parent_method = 'children', $crumb = 'public.chunks.filter', $curcrumb = 'public.chunks.curfilter')
    {
        $crumbs = '';
        $children = $parent -> $parent_method;
        $name = explode('.', request()->route()->getName())[1];
        $type = DataType::where('slug', $name) -> firstOrFail();
        if($model->id === $parent->id)
            $root_crumb = $curcrumb;
        else
            $root_crumb = $crumb;
        $crumbs .= View::make($root_crumb, ['link' => route('public.'.$type->slug, ['slug' => $parent->slug]), 'page' => $parent]);
        if(count($children) > 0) {
            $crumb_temp = '';
            foreach ($children as $child){
                if($model->id === $child->id)
                    $crumb_temp = $curcrumb;
                else
                    $crumb_temp = $crumb;

                $crumbs .= View::make($crumb_temp, ['link' => route('public.' . $type->slug, ['slug' => $model->slug]), 'page' => $child]);
            }

        }
        return $crumbs;
    }

}

