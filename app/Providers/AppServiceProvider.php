<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use TCG\Voyager\Models\Category;
use App\PortpholioCategory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		  // Enabling query log support

        \DB::enableQueryLog();

        // Setting string length of DB columns for migration
        Schema::defaultStringLength(191);

        Carbon::setLocale('ru');


        $myhelper = app_path('Helper.php');
        if(file_exists($myhelper))
            require_once($myhelper);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //$this->app->bind(Category::class, PortpholioCategory::class);
    }
}
