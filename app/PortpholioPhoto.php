<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PortpholioItem;

class PortpholioPhoto extends Model
{

    //
    protected $fillable = [
        'portpholio_item_id',
        'user_id',
        'name',
        'path',
        'size'

    ];


    protected $casts = [
        'user_id' => 'integer',
        'portpholio_item_id' => 'integer'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($photo) {
            $id = \Auth::user()->id;
            if($photo->portpholio_item_id !== 0){
                $portpholio = $photo->portpholio;
                if($portpholio){
                    if($portpholio->user_id !== $id) {
                        $photo->portpholio_item_id = 0;
                        $photo->save();
                    }
                }

            }

        });

        static::deleted(function($photo){
            \Storage::disk('public')->delete($photo->attributes['path']);
        });

    }

    public function portpholio_item(){
        return $this->belongsTo('App\PortpholioItem');
    }

    public function getPathAttribute($value)
    {
        return \Storage::disk('public')->url($value);
    }

}
