<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class PortpholioItem extends Model
{
    use Translatable,
        Resizable,
        HasRelationships;
    //
    protected $translatable = ['name', 'seo_title', 'body', 'slug', 'meta_description', 'meta_keywords'];

    protected $fillable= [
        'user_id',
        'category_id',
        'name',
        'body',
        'options',
        'slug',
        'meta_description',
        'meta_keywords',
        'seo_title',
        'is_active'
    ];

    /*-public function category()
    {
        return $this->belongsTo(Voyager::modelClass('Category'));
    }*/

    public function photos(){
        return $this->hasMany('App\PortpholioPhoto');
    }

    public function category(){
        return $this->belongsTo('App\PortpholioCategory');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($portpholio){
            if(count($portpholio->photos) > 0){
                $photos = $portpholio->photos;
                foreach($photos as $photo){
                    $photo -> delete();
                }
            }
        });

    }

}
