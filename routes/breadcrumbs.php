<?php

use TCG\Voyager\Models\DataType;
use App\PortpholioCategory;
use Illuminate\Database\Eloquent\Model;

if (! function_exists('parentRec')) {

    function parentRec(Model $model, $parent_method = 'parent', $table = false)
    {
        $table = $model -> getTable();
        $type = DataType::where('slug', $table) -> firstOrFail();
        $result = array();

        $parent = $model->$parent_method;
        if($parent){
            $result = parentRec($parent);
        }

        $result[] =  array($model->name, route('public.' . $type->slug, ['slug' => $model->slug]));

        return $result;
    }

}


Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', '/');
});

Breadcrumbs::register('about', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('home');
    $name = explode('.', request()->route()->getName())[1];
    $type = DataType::where('slug', $name) -> firstOrFail();
    $breadcrumbs->push($page->title, route('public.'.$type->slug, ['slug' => $page->slug]));
});

Breadcrumbs::register('service', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('home');
    $name = explode('.', request()->route()->getName())[1];
    $type = DataType::where('slug', $name) -> firstOrFail();
    $breadcrumbs->push($page->title, route('public.'.$type->slug, ['slug' => $page->slug]));
});


Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('home');
    $name = explode('.', request()->route()->getName())[1];
    $type = DataType::where('slug', $name) -> firstOrFail();
    $parent = $category->parent;
    if($parent)
        $breadcrumbs->push($parent->name, route('public.'.$type->slug, ['slug' => $parent->slug]));

    $breadcrumbs->push($category->name, route('public.'.$type->slug, ['slug' => $category->slug]));
});

Breadcrumbs::register('portpholio', function ($breadcrumbs, $portpholio) {

    $breadcrumbs->parent('home');

    $parent = $portpholio->category;

    if($parent) {
        $result = parentRec($parent);
        foreach ($result as $item)
            $breadcrumbs->push($item[0], $item[1]);
    }
    $name = explode('.', request()->route()->getName())[1];
    $type = DataType::where('slug', $name) -> firstOrFail();
    $breadcrumbs->push($portpholio->name, route('public.'.$type->slug, ['slug' => $portpholio->slug]));

});
