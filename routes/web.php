<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Page;
use Illuminate\Http\Request;



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::group(['middleware' => 'admin.user'], function () {
        Route::post('portpholio-photo/create', 'PortpholioItemController@storephoto')->name('voyager.portpholio-photo.create');
        Route::delete('portpholio-photo/{photo}', 'PortpholioItemController@deletephoto')->name('voyager.portpholio-photo.delete');
    });

});

Route::group(['as' => 'public.'], function () {

        $types_name = ['pages', 'categories', 'portpholio_items'];

        $types = DataType::whereIn('name', $types_name) -> get();
    //->where($this->getSlugKeyName(), $slug);
        foreach ($types as $dataType) {

            Route::get($dataType->slug.'/{slug}', function(Request $request, $slug){
                $name = explode('.', $request->route()->getName())[1];
                $type = DataType::where('slug', $name) -> firstOrFail();
                //return $type->model_name;

                $model = app($type->model_name) -> where('slug', $slug)-> firstOrFail();
                $slug = $type->slug;

                return view('public.'.$slug.'.page', array('page' => $model));

            })->name($dataType->slug);
        }

        $page = Page::find(1);

        if($page){
            Route::get('/', function() use ($page) {
                return view('public.pages.home', compact('page'));
            });
        }
        else{
            Route::get('/', function(){
                return view('welcome');
            });

        }

    Route::post('mail', 'ContactMailController')->name('contact.mail');
});



