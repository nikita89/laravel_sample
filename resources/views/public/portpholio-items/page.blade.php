@extends('public.base')

@section('content')
    <!-- Page Content ss-->
    <main class="page-content">
        <!-- Page Header-->
        @include('public.includes.brand')
        <section class="section-98 section-md-110">
            <div class="shell-fluid">
                <h1 class="text-bold">{{$page->name}}</h1>
                <ul class="p list-inline list-inline-dashed offset-top-24">
                    {{ Breadcrumbs::view('public.chunks.crumb', 'portpholio', $page) }}

                </ul>
                <!-- single project-->
                <section class="section-98 section-md-110">
                    <div class="shell-fluid">
                        <div class="range range-xs-center">
                            <div class="cell-xs-9 cell-xl-5 cell-md-5 cell-lg-5 cell-xl-push-2">
                                <!-- Swiper-->
                                <div data-height="67%" data-min-height="400px" data-dots="true" class="swiper-container swiper-slider">
                                    <div class="swiper-wrapper">
                                        @if(count($page->photos)>0)
                                            @foreach($page->photos as $photo)
                                                <div data-slide-bg="{{$photo->path}}" class="swiper-slide swiper-slide-overlay-disable"></div>
                                            @endforeach
                                        @endif
                                        [[!Gallery? &album=`[[*album]]`&thumbTpl=`album_portfolio`&imageWidth=`1387`&imageHeight=`925`&useCss=`0`]]
                                    </div>
                                    <!-- Swiper Pagination-->
                                    <div class="swiper-pagination swiper-pagination-type-1"></div>
                                </div>
                            </div>
                            <div class="cell-xl-3 cell-xs-9 inset-xl-left-80 text-left cell-xl-push-1 offset-top-66 offset-xl-top-0">
                                <div class="range">
                                    <div class="cell-sm-6 cell-xl-12">
                                        <time datetime="2016-01-01"><span style="font-size:18px" class="icon icon-xxs fa fa-calendar text-middle"></span> <span class="text-middle">{{$page->created_at->diffForHumans()}}</span>
                                        </time>
                                        <h4 class="text-dark font-default offset-top-50">Об альбоме</h4>
                                        <p>
                                            {!! $page->body !!}
                                        </p>
                                    </div>
                                    <div class="cell-sm-6 cell-xl-12 offset-top-50 offset-sm-top-0 offset-xl-top-66">
                                        <h4 class="text-dark font-default">Информация</h4>
                                        <ul class="list list-marked">
                                            @if($page->options)
                                                @foreach(json_decode($page->options) as $option)
                                                    <li>{{$option}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </main>

@stop