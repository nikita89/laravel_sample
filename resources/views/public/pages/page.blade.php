@extends('public.base')


@switch($page->id)
    @case(2)
        @section('content')

            @include('public.includes.about', ['page' => $page])

        @stop
    @break

    @case(3)
        @section('content')

            @include('public.includes.service', ['page' => $page])

        @stop
    @break

    @case(4)
        @section('content')

            @include('public.includes.contact', ['page' => $page])

        @stop
    @break

    @default

@endswitch
