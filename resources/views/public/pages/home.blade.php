@extends('public.base')

@section('content')
    <!-- Page Content-->
    <main class="page-content">
        <div class="section-relative section-cover">
            <!-- Swiper-->
            <div style="height: 400px;" data-height="100vh" data-min-height="400px" data-dots="true" class="swiper-container swiper-slider swiper-container-horizontal">
                <div style="transition-duration: 0ms; transform: translate3d(-1280px, 0px, 0px);" class="swiper-wrapper">
                    <div style="background-image: url({{asset('images/image-01-1920x1080.jpg')}}); background-size: cover; width: 1280px;" data-slide-bg="{{asset('images/image-01-1920x1080.jpg')}}" class="swiper-slide swiper-slide-overlay-disable swiper-slide-center"></div>
                    <div style="background-image: url({{asset('images/image-02-1920x1080.jpg')}}); background-size: cover; width: 1280px;" data-slide-bg="{{asset('images/image-02-1920x1080.jpg')}}" class="swiper-slide swiper-slide-overlay-disable swiper-slide-center"></div>
                    <div style="background-image: url({{asset('images/image-03-1920x1080.jpg')}}); background-size: cover; width: 1280px;" data-slide-bg="{{asset('images/image-03-1920x1080.jpg')}}" class="swiper-slide swiper-slide-overlay-disable swiper-slide-center"></div>
                </div>
                <!-- Swiper Pagination-->
                <div class="swiper-pagination swiper-pagination-type-1 swiper-pagination-clickable"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>
            </div>
            <div class="section-center-absolute offset-top-20 offset-md-top-0">
                <div id="fadin_name" data-text="{{setting('site.author_name')}}"></div>
                <div class="offset-top-20 offset-md-top-41">
                    <h5 class="text-uppercase font-default text-spacing-120">Фотограф, Москва</h5> </div>
                <div class="shell offset-top-30 offset-md-top-50">
                    <div class="range range-xs-center">
                        <div class="cell-sm-9 cell-md-8 cell-lg-6 cell-xl-6"><a href="{{ route('public.categories', ['slug' => getFieldById('2', 'App\PortpholioCategory')]) }}" class="btn btn-lg btn-block btn-primary btn-rect text-spacing-120 text-regular">Все работы</a></div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@stop