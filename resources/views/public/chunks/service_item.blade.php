<div class="cell-lg-6 cell-xl-4 offset-top-41">
    @if($preview->photos)
        <a href="{{$preview->photos()->first()->path}}" data-size="442x442" data-photo-swipe-item="" class="thumbnail-winston">
            <figure><img width="442" height="442" src="{{$preview->photos()->first()->path}}" alt="{{$preview->name}}">
                <figcaption class="text-left">
                    <h3 class="thumbnail-winston-title">{{$preview->name}}</h3>
                    <div class="offset-top-50 veil reveal-lg-inline-block">
                        @if($preview->options)
                            @foreach(json_decode($preview->options) as $option)
                                <p>{{ $option }}</p><br>
                            @endforeach
                        @endif
                    </div>
                </figcaption>
            </figure>
        </a>
    @endif
</div>