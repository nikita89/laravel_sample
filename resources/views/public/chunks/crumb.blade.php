{{--<li><a href="{{$link}}">{{$page->title ? $page->title : $page->name}}</a></li>--}}

@if (count($breadcrumbs))

        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li>{{ $breadcrumb->title }}</li>
            @endif

        @endforeach

@endif