@extends('public.base')

@section('content')
    <!-- Page Content-->
    <main class="page-content">
        <!-- Page Header-->
        @include('public.includes.brand')
        <!-- Portfolio-->
        <section class="section-85">
            <div class="shell-fluid">
                <h1 class="text-bold">{{$page->name}}</h1>
                <ul class="p list-inline list-inline-dashed offset-top-24">
                    {{ Breadcrumbs::view('public.chunks.crumb', 'category', $page) }}
                </ul>
                <div class="offset-top-30">
                    <div class="isotope-wrap">
                        <div class="row">
                            <!-- Isotope Filters-->
                            <div class="col-lg-12">	<!--<div class="col-lg-3">-->
                                <div class="isotope-filters isotope-filters-vertical">
                                    <h4 class="text-uppercase text-bold isotope-filters-title offset-top-34">Категории</h4>
                                    <ul class="list-inline list-inline-sm">
                                        <li class="veil-md">
                                            <p>Выберите категорию:</p>
                                        </li>
                                        <li class="section-relative">
                                            <button data-custom-toggle="isotope-53" data-custom-toggle-disable-on-blur="true" class="isotope-filters-toggle btn btn-sm btn-primary">Фильтер<span class="caret"></span></button>
                                            <ul id="isotope-53" class="list-sm-inline isotope-filters-list">
                                                @if($page->parent)
                                                    {!! filterCrumb($page, $page->parent) !!}
                                                @else
                                                    {!! filterCrumb($page, $page) !!}
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Isotope Content-->
                            <div class="col-lg-12 offset-lg-top-0 offset-top-34">
                                <div data-isotope-layout="masonry" data-isotope-group="gallery" class="isotope">
                                    <div data-photo-swipe-gallery="gallery" class="row">
                                        @php
                                            $category_ids = App\PortpholioCategory::where(['id' => 2])->
                                                orWhere(['parent_id' => 2])->pluck('id')->toArray();
                                            //print_r($category_ids);
                                            $portpholios = App\PortpholioItem::with(['category','photos']) -> whereIn('category_id', $category_ids)->
                                                get();

                                        @endphp
                                        @if(count($portpholios)>0)
                                            @foreach($portpholios as $portpholio)
                                                @include('public.includes.portpholiopreview', ['preview' => $portpholio])
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

@stop