<div style="height: 0px;" class="rd-navbar-wrap">
    <nav class="rd-navbar rd-navbar-sidebar-toggle rd-navbar-light rd-navbar-fixed" data-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-stick-up="false" data-lg-hover-on="false">
        <div class="rd-navbar-inner">
            <!-- RD Navbar Panel-->
            <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
            </div>
            <div class="rd-navbar-menu-wrap">
                <div class="rd-navbar-nav-wrap">
                    <div class="rd-navbar-mobile-scroll-holder">
                        <div class="rd-navbar-mobile-scroll">
                            <!--Navbar Brand Mobile-->
                            <div class="rd-navbar-mobile-brand">
                                <a href="/"><img style="margin-top: -5px;margin-left: -15px;" src="{{asset('image/logo-dark.png')}}" alt="" height="31" width="138"></a>
                            </div>
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <li class="first active"><a href="/" title="Главная">Главная</a></li>
                                <li><a href="{{ route('public.pages', ['slug' => getFieldById('2', 'TCG\Voyager\Models\Page')]) }}" title="Обо мне">Обо мне</a></li>
                                <li class="rd-navbar--has-dropdown rd-navbar-submenu"><a href="{{ route('public.categories', ['slug' => $root->slug]) }}" title="Портфолио">Портфолио</a><ul class="rd-navbar-dropdown" style="">
                                        <li class="first"><a href="{{ route('public.categories', ['slug' => $root->slug]) }}" title="Все категории">{{ $root->name }}</a></li>
                                        @if(count($children) > 0)
                                            @foreach($children as $child)
                                                <li><a @if ($loop->last) class="last" @endif href ="{{ route('public.categories', ['slug' => $child->slug]) }}" title="Мода">{{$child->name}}</a></li>
                                            @endforeach
                                        @endif

                                    </ul><span class="rd-navbar-submenu-toggle"></span></li>
                                <li class="rd-navbar--has-dropdown rd-navbar-submenu"><a href="{{ route('public.pages', ['slug' => getFieldById('3', 'TCG\Voyager\Models\Page')]) }}" title="Услуги">Услуги</a></li>
                                <li class="last"><a href="{{ route('public.pages', ['slug' => getFieldById('4', 'TCG\Voyager\Models\Page')]) }}" title="Контакты">Контакты</a></li>

                            </ul>
                            <div class="rd-navbar-footer">
                                <div class="rd-navbar-address">
                                    <dl class="offset-top-0"> <dt><span class="mdi mdi-phone"></span></dt>
                                        <dd><a href="callto:{{setting('site.phone')}}">{{setting('site.phone')}}</a></dd>
                                    </dl>
                                    <dl> <dt><span class="mdi mdi-email-open"></span></dt>
                                        <dd><a href="mailto:{{setting('site.email')}}">{{setting('site.email')}}</a></dd>
                                    </dl>
                                </div>
                                <ul class="list-inline">
                                    <li>
                                        <a href="https://ru-ru.facebook.com/" class="icon fa fa-facebook icon-xxs icon-circle icon-darker-filled"></a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/" class="icon fa fa-twitter icon-xxs icon-circle icon-darker-filled"></a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/collections/featured" class="icon fa fa-google-plus icon-xxs icon-circle icon-darker-filled"></a>
                                    </li>
                                    <li>
                                        <a href="https://500px.com/" class="icon fa fa fa-500px icon-xxs icon-circle icon-darker-filled"></a>
                                    </li>
                                    <li>
                                        <a href="https://www.behance.net/" class="icon fa fa fa-behance icon-xxs icon-circle icon-darker-filled"></a>
                                    </li>
                                </ul>
                                <p class="small">{{setting('site.footer_name')}} © <span id="copyright-year">2016</span> .</a>
                                    <!-- {%FOOTER_LINK}-->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>