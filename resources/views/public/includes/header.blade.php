<!DOCTYPE html>
<html class="wide wow-animation smoothscroll scrollTo desktop landscape rd-navbar-fixed-linked" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!-- Site Title-->
    <title>{{$page -> title ? $page -> title : $page -> name}}</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="{{$page -> meta_keywords}}">
    <meta name="description" content="{{$page -> meta_description}}">
    <base href="/">
    <link rel="icon" href="{{asset('image/favicon.ico')}}" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/css.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>

<body>
<!-- Page-->
<div class="page text-center">
    <!-- Page Head-->
    <header class="page-head">
        <!-- RD Navbar Transparent-->
        @php
        $root_category = App\PortpholioCategory::findOrFail(2);
        $children = $root_category -> children;
        @endphp
        @include('public.includes.navigation', ['root' => $root_category, 'children' => $children])

    </header>