<!-- NICK ANRI-->
<section>
    <div data-on="false" data-md-on="true" class="rd-parallax">
        <div data-speed="0.45" data-type="media" data-url="{{Storage::disk('public')->url(setting('site.inner_brand'))}}" class="rd-parallax-layer"></div>
        <div data-speed="0.3" data-type="html" data-fade="true" class="rd-parallax-layer">
            <div class="shell section-85 fadin_inner"><a href="[[~1]]"><div id="fadin_name" data-text="{{setting('site.author_name')}}"></div></a></div>
        </div>
    </div>
</section>