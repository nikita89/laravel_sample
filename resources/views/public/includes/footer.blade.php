</div>
<!-- Global Mailform Output-->
<div id="form-output-global" class="snackbars"></div>

<!-- PhotoSwipe Gallery-->
<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
                <button title="Share" class="pswp__button pswp__button--share"></button>
                <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
                <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
            <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

<!-- Java script-->
<script src="{{asset('js/core.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script>
    var source,dest,len,now=0,delay=200,letters=1, cur_text = "";
    function show_text()
    {
        source = $("#fadin_name").attr('data-text');
        dest = $("#fadin_name");
        len = source.length;
        show();
    }

    function show()
    {
        cur_text = dest.text();
        cur_text += source.substr(now,letters);
        dest.text(cur_text);
        now+=letters;

        if(now<len)
            setTimeout("show()",delay);
    }
    $(document).ready(function(){
        show_text();
    });
</script>
</body>
<!-- Google Tag Manager -->

</html>