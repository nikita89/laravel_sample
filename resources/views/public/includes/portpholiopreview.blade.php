<div data-filter="{{$preview->category_id}}" class="col-xs-12 col-sm-6 col-lg-4 isotope-item">
<!-- Thumbnail Apollo-->
    @if(count($preview->photos) > 0)
        <a href="{{ route('public.portpholio-items', ['slug' => $preview->slug]) }}" class="thumbnail-apollo">
            <figure><img src="{{$preview->photos->first()->path}}" alt="{{$preview->name}}"/>
                <figcaption>
                    <div>
                        <h3 class="thumbnail-apollo-title">{{$preview->name}}</h3>
                    </div>
                    <p class="small">{!! str_limit($preview->body, 50) !!}</p><span class="icon icon-xxs veil reveal-sm-inline-block fa-arrow-right"></span>
                </figcaption>
            </figure>
        </a>
    @endif
</div>