<!-- Page Content-->
<main class="page-content">
    <!-- Page Header-->
    @include('public.includes.brand')
    <!-- About Me-->
    <section class="section-85">
        <div class="shell">
            <h1 class="text-bold">{{$page -> name}}</h1>
            <ul class="p list-inline list-inline-dashed offset-top-24">
                {{ Breadcrumbs::view('public.chunks.crumb', 'about', $page) }}
            </ul>
            <!-- About Me-->
            <section>
                <div class="range range-xs-center offset-top-66">
                    <div class="cell-lg-5 text-lg-right"><img src="{{Storage::disk('public')->url($page->image)}}" width="442" height="480" alt="" class="img-responsive center-block">
                        <div class="range range-xs-center offset-top-34">
                            <div class="cell-sm-8 cell-md-6 cell-lg-12">
                                <div class="inset-xs-left-80 inset-xs-right-80"><a href="{{ route('public.pages', ['slug' => getFieldById('4', 'TCG\Voyager\Models\Page')]) }}" class="btn btn-lg btn-block btn-primary btn-rect text-spacing-120 text-regular">Связаться</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="cell-sm-10 text-left cell-md-9 offset-top-41 cell-lg-5 offset-lg-top-0">
                        <div class="inset-sm-left-30">
                            {!! $page -> body !!}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>