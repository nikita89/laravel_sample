<!-- Page Content-->
<main class="page-content">
    <!-- Page Header-->
    @include('public.includes.brand')
    <!-- Services-->
    <section class="section-85">
        <div class="shell-fluid">
            <h1 class="text-bold">{{$page -> title}}</h1>
            <ul class="p list-inline list-inline-dashed offset-top-24">
                {{ Breadcrumbs::view('public.chunks.crumb', 'service', $page) }}
            </ul>
            <!-- Services-->
            <section>
                <div class="range range-xs-center offset-top-66">
                    <div class="cell-xs-10 cell-sm-8 cell-md-7 cell-lg-7 cell-xl-3 text-xl-left">
                        <div class="inset-xl-left-100 inset-xl-right-20">
                            {!! $page -> body !!}
                        </div>
                    </div>
                    <div class="cell-xs-10 cell-sm-7 cell-md-6 cell-lg-8 cell-xl-9 offset-top-41 offset-xl-top-0">
                        <div class="range">
                            @php
                                $portpholios = App\PortpholioItem::with(['category','photos']) -> where('category_id', 7)->get();
                            @endphp
                            @if(count($portpholios)>0)
                                @foreach($portpholios as $portpholio)
                                    @include('public.chunks.service_item', ['preview' => $portpholio])
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>