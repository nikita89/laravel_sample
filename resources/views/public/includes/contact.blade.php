<!-- Page Content-->
<main class="page-content">
    <!-- Page Header-->
    @include('public.includes.brand')
    <!-- Contact Me-->
    <section class="section-top-85">
        <div class="shell">
            <h1 class="text-bold">{{$page->title}}</h1>
            <ul class="p list-inline list-inline-dashed offset-top-24">
                {{ Breadcrumbs::view('public.chunks.crumb', 'service', $page) }}
            </ul>
        </div>
        <!-- Contact Me-->
        <section>
            <div class="shell-fluid">
                <div class="range range-xs-center text-sm-left offset-top-50">
                    <div class="cell-xs-10 cell-sm-8 cell-md-8 cell-xl-6">
                    </div>
                </div>
                <div class="range range-xs-center text-sm-left offset-top-50">
                    <div class="cell-xs-10 cell-sm-12 cell-md-8 cell-lg-5 cell-xl-4">
                        <!-- RD Mailform-->
                        @if(count($errors)>0)
                            @include('public.includes.error')
                        @endif
                        <form data-form-output="form-output-global" data-form-type="contact" method="post" action="{{route('public.contact.mail')}}" class="rd-mailform text-left">
                           
                            <div class="range">
                                <div class="cell-lg-6">
                                    <div class="form-group">
                                        <label for="contact-me-name" class="form-label rd-input-label">Имя</label>
                                        <input id="contact-me-name" name="name" data-constraints="@Required" required class="form-control form-control-has-validation form-control-last-child" type="text"><span class="form-validation"></span>
                                    </div>
                                </div>
                                <div class="cell-lg-6 offset-top-20 offset-lg-top-0">
                                    <div class="form-group">
                                        <label for="contact-me-phone" class="form-label rd-input-label">Телефон</label>
                                        <input id="contact-me-phone" name="phone" required data-constraints="@Required" class="form-control form-control-has-validation form-control-last-child" type="text"><span class="form-validation"></span>
                                    </div>
                                </div>
                                <div class="cell-lg-12 offset-top-20 offset-lg-top-30">
                                    <div class="form-group">
                                        <label for="contact-me-email" class="form-label rd-input-label">E-Mail</label>
                                        <input id="contact-me-email" name="email" required data-constraints="" class="form-control form-control-has-validation form-control-last-child" type="text"><span class="form-validation"></span>
                                    </div>
                                </div>
                                <div class="cell-lg-12 offset-top-20 offset-lg-top-30">
                                    <div class="form-group">
                                        <label for="contact-me-message" class="form-label rd-input-label">Сообщение</label>
                                        <textarea id="contact-me-message" name="message" data-constraints="@Required" style="height: 150px" class="form-control form-control-has-validation form-control-last-child"></textarea><span class="form-validation"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="range range-xs-center range-lg-left text-center text-lg-left offset-top-30">
                                <div class="cell-xs-10 cell-sm-6 cell-md-7 cell-lg-8">
                                    <button type="submit" value="true" name="contact" class="btn btn-lg btn-block btn-primary btn-rect text-spacing-120 text-regular">Отправить сообщение</button>
                                </div>
                            </div>
							{{ csrf_field() }}
                        </form>

                    </div>
                    <div class="cell-xs-10 cell-sm-8 cell-md-8 cell-lg-3 cell-xl-2 offset-top-50 offset-lg-top-0">
                        <div class="inset-xl-right-20">
                            <address class="contact-info text-left">
                                <div class="unit unit-spacing-sm unit-horizontal">
                                    <div class="unit-left"><span class="icon icon-xxs fa fa-map-marker text-white"></span></div>
                                    <div class="unit-body p offset-top-0"><span> {{setting('site.address')}}</span></div>
                                </div>
                                <div class="p unit unit-spacing-xs unit-horizontal offset-top-20">
                                    <div class="unit-left"><span class="icon icon-xxs fa fa-phone text-white"></span></div>
                                    <div class="unit-body"><a href="callto:{{setting('site.phone')}}">{{setting('site.phone')}}</a></div>
                                </div>
                                <div class="p unit unit-spacing-xs unit-horizontal offset-top-10">
                                    <div class="unit-left"><span class="icon icon-xxs fa fa-envelope text-white"></span></div>
                                    <div class="unit-body"><a href="mailto:{{setting('site.email')}}">{{setting('site.email')}}</a></div>
                                </div>
                            </address>
                        </div>
                        <div class="text-center text-lg-left offset-top-30">
                            <ul class="list-inline">
                                <li>
                                    <a href="https://ru-ru.facebook.com/" class="icon fa fa-facebook icon-xxs icon-circle icon-darker-filled"></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/" class="icon fa fa-twitter icon-xxs icon-circle icon-darker-filled"></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/collections/featured" class="icon fa fa-google-plus icon-xxs icon-circle icon-darker-filled"></a>
                                </li>
                                <li>
                                    <a href="https://500px.com/" class="icon fa fa fa-500px icon-xxs icon-circle icon-darker-filled"></a>
                                </li>
                                <li>
                                    <a href="https://www.behance.net/" class="icon fa fa fa-behance icon-xxs icon-circle icon-darker-filled"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section RD Google Map-->
        <section class="offset-top-98">
            <!-- RD Google Map-->
            <div id="rd-yandex-map">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=tD5q_jDVCQq9qdKXbaxJ90yaE-jXoIVN&amp;width=100%&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
            </div>

        </section>
    </section>
</main>