@extends('voyager::posts.edit-add')

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <link rel="stylesheet" href="{{ asset('css/dropzone_basic.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">

            {!! $photos !!}
            <form id="post_dropzone"  class="mydropzone" action="{{ route('voyager.portpholio-photo.create') }}" target="form_target" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
            </form>
            <form class="form-edit-add dropzonepost" role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.portpholio-items.update', $dataTypeContent->id) }}@else{{ route('voyager.portpholio-items.store') }}@endif" method="POST" enctype="multipart/form-data">

                <!-- PUT Method if we are editing -->
                @if(isset($dataTypeContent->id))
                    {{ method_field("PUT") }}
                @endif
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-8">
                        <!-- ### TITLE ### -->
                        <div class="panel">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="voyager-character"></i> {{ __('voyager.portpholio.title') }}
                                    <span class="panel-desc"> {{ __('voyager.portpholio.title_sub') }}</span>
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'name',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'name')
                                ])
                                <input type="text" class="form-control" id="title" name="name" placeholder="{{ __('voyager.generic.title') }}" value="@if(isset($dataTypeContent->title)){{ $dataTypeContent->title }}@endif">
                            </div>
                        </div>

                        <!-- ### CONTENT ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-book"></i> {{ __('voyager.post.content') }}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                                </div>
                            </div>
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name'  => 'body',
                                '_field_trans' => get_field_translations($dataTypeContent, 'body')
                            ])
                            <textarea class="form-control richTextBox" id="richtextbody" name="body" style="border:0px;">@if(isset($dataTypeContent->body)){{ $dataTypeContent->body }}@endif</textarea>
                        </div><!-- .panel -->



                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Fields</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @php
                                    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                    $options_row = $dataTypeRows -> where('field', 'options')->first();

                            @endphp


                            <div class="form-group>
                                <label for="name">{{ $options_row->display_name }}</label>
                            @if(!empty($dataTypeContent->options))
                                @php
                                    $options = json_decode($dataTypeContent->options, true);
                                @endphp
                                @foreach($options as $option)
                                    <input type="text" class="form-control" name="options[]" value="{{ $option }}">
                                @endforeach
                            @else
                                <input type="text" class="form-control" name="options[]" value="{{$dataTypeContent->options}}">
                            @endif
                                <button type="submit" id="add_option" class="btn btn-primary">{{ __('voyager.portpholio.add_option') }}</button>
                            </div>


                        </div>
                    </div>


                </div>

                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager.post.details') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">{{ __('voyager.post.slug') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'slug',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                ])
                                <input type="text" class="form-control" id="slug" name="slug"
                                       placeholder="slug"
                                       {{!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}}
                                       value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                            </div>
                            @php
                                $active_row = $dataTypeRows -> where('field', 'is_active')->first();
                            @endphp
                            <div class="form-group>
                                 <label for="name">{{ $active_row->display_name }}</label>
                                {!! app('voyager')->formField($active_row, $dataType, $dataTypeContent) !!}
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('voyager.post.category') }}</label>
                                <select class="form-control" name="category_id">
                                    @foreach(TCG\Voyager\Models\Category::all() as $category)
                                        <option value="{{ $category->id }}"@if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <!-- ### SEO CONTENT ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager.post.seo_content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">{{ __('voyager.post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_description',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                ])
                                <textarea class="form-control" name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('voyager.post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                ])
                                <textarea class="form-control" name="meta_keywords">@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('voyager.post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                ])
                                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title" value="@if(isset($dataTypeContent->seo_title)){{ $dataTypeContent->seo_title }}@endif">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">
                @if(isset($dataTypeContent->id)){{ __('voyager.post.update') }}@else <i class="icon wb-plus-circle"></i> {{ __('voyager.post.new') }} @endif
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>

    </div>
@stop

@section('javascript')

    <script>
        $('document').ready(function () {


            $('#slug').slugify();

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('#add_option').click(function(e){
                e.preventDefault();
                var new_el = $(this).prev().clone().val('');
                new_el.insertBefore($(this));
            })
        });

    </script>
@stop
